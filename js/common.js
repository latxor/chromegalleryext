//Función encargada de realizar el comando copiar
//Formateamos la cadena que se va a copiar en el clipboard y luego la copiamos
function copyCommand(ulrsArray)
{
     document.oncopy = function(event) {
      var result = "";
      ulrsArray.forEach(function(element) {
          result = result + element + '\n';
      }, this);
     
      event.clipboardData.setData('text/plain', result);    
      event.preventDefault();
    };

    document.execCommand("Copy", false,false);
}

//Función encargada de definir el numero de las images actualmente agregadas
function setBadge(stringBadge)
{
     chrome.browserAction.setBadgeText({text: stringBadge});
}